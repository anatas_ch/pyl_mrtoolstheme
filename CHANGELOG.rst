=========
Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`__,
and this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`__.


[Unreleased]
============

Added
-----

Changed
-------
- format of the sidebar directive
- minimal Python version: 3.12

Fixed
-----

- #7: Sidebar Title: top margin


[0.5.1] - 2023-02-04
====================

Fixed
-----
- description on PyPi


[0.5.0] - 2023-02-04
====================

Changed
-------
- minimal Python version: 3.9
- formatting long index entries

Fixed
-----
- #3: Numbered lists in admonitions
- #4: Spacing around figure caption
- #5: Spacing around table captions
- #6: Harmonize horizontal spacing lists


[0.4.0] - 2022-02-18
====================

Added
-----
- formats for `versionadded`, `versionchanged` and `deprecated`

Changed
-------
- formats for the python domain


[0.3.0] - 2021-11-05
====================

Changed
-------
- Table format: add top and bottom borders
- minimal Python version: 3.6

Fixed
-----
- Doc: wrong version print out
- #2: Text as code formatted in a ``code-block`` ``caption``


[0.2.0] - 2021-06-14
====================

Added
-----
- This CHANGELOG file.

Changed
-------
- Documentation: Link to Read the Docs

Fixed
-----
- #1: ``code-block`` in a ``admonition`` is not rendered properly


[0.1.0] - 2021-06-04
====================

Added
-----
- first public release.
