[project]
name = "mrtoolstheme"
authors = [{ name = "Michael Rippstein", email = "info@anatas.ch" }]
maintainers = [{ name = "Michael Rippstein", email = "info@anatas.ch" }]
description = "a Sphinx theme"
dynamic = ["version", "readme"]
license = { text = "AGPL-3.0-or-later" }
classifiers = [
    "Development Status :: 4 - Beta",
    "Intended Audience :: Developers",
    "Intended Audience :: End Users/Desktop",
    "Intended Audience :: System Administrators",
    "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
    "Operating System :: OS Independent",
    "Environment :: Console",
    "Environment :: Web Environment",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3.12",
    "Programming Language :: Python :: Implementation :: CPython",
    "Programming Language :: Python :: Implementation :: PyPy",
    "Framework :: Sphinx :: Theme",
    "Topic :: Documentation",
    "Topic :: Documentation :: Sphinx",
    "Topic :: Software Development :: Documentation",
    "Topic :: Text Processing :: Filters",
    "Topic :: Utilities",
]
requires-python = ">=3.12"
dependencies = ["Sphinx>=8.1.3", "docutils>=0.21.2"]

[project.urls]
documentation = "https://mrtoolstheme.readthedocs.io/"
sourc = "https://gitlab.com/anatas_ch/pyl_mrtoolstheme"
issues = "https://gitlab.com/anatas_ch/pyl_mrtoolstheme/-/issues"

[project.entry-points."sphinx.html_themes"]
mrtools = "mrtoolstheme"

[project.entry-points."pygments.styles"]
mrtools = "mrtoolstheme.support:MrToolsStyle"

[build-system]
requires = ["setuptools>=75", "wheel>=0.45", "setuptools_scm>=8"]

[tool.setuptools.packages.find]
where = ['src']

[tool.setuptools.package-data]
mrtoolstheme = ["*.html", "*.conf", "static/*.css_t"]

[tool.setuptools.dynamic]
readme = { file = ["README.rst", "CHANGELOG.rst"] }

[tool.setuptools_scm]

# == RUFF ==
[tool.ruff]
# Allow lines to be as long as 111.
line-length = 111
# Always generate Python 3.12-compatible code.
target-version = "py312"
# Enable preview features.
preview = true

[tool.ruff.format]
# Use `\n` line endings for all files
line-ending = "lf"

# Prefer single quotes over double quotes.
quote-style = "single"

# Enable reformatting of code snippets in docstrings.
# try the docstring code formatter if all other issues are done
docstring-code-format = false

# Format all docstring code snippets with a line length of 60.
docstring-code-line-length = 60

# Files to exclude from formating
exclude = ["*.pyi"]

[tool.ruff.lint]
select = [
    "A",    # flake8-builtins
    "AIR",  # Airflow
    "ANN",  # flake8-annotations
    "ARG",  # flake8-unused-arguments
    "B",    # flake8-bugbear
    "BLE",  # flake8-blind-except
    "C4",   # flake8-comprehensions
    "C90",  # mccabe
    "COM",  # flake8-commas
    "CPY",  # flake8-copyright
    "D",    # pydocstyle
    "DOC",  # pydoclint
    "DTZ",  # flake8-datetimez
    "E",    # pycodestyle error
    "EM",   # flake8-errmsg
    "ERA",  # eradicate
    "F",    # pyflakes
    "FA",   # flake8-future-annotations
    "FAST", # FastAPI
    "FBT",  # flake8-boolean-trap
    "FIX",  # flake8-fixme
    "FLY",  # flynt
    "FURB", # refurb
    "G",    # flake8-logging-format
    "I",    # isort
    "ICN",  # flake8-import-conventions
    "INP",  # flake8-no-pep420
    "ISC",  # flake8-implicit-str-concat
    "LOG",  # flake8-logging
    "N",    # pep8-naming
    "NPY",  # NumPy-specific rules
    "PERF", # Perflint
    "PGH",  # pygrep-hooks
    "PIE",  # flake8-pie
    "PL",   # Pylint
    "PT",   # flake8-pytest-style
    "PTH",  # flake8-use-pathlib
    "PYI",  # flake8-pyi
    "Q",    # flake8-quotes
    "RET",  # flake8-return
    "RSE",  # flake8-raise
    "RUF",  # Ruff-specific rules
    "S",    # flake8-bandit
    "SIM",  # flake8-simplify
    "SLF",  # flake8-self
    "SLOT", # flake8-slots
    "T10",  # flake8-debugger
    "T20",  # flake8-print
    "TCH",  # flake8-type-checking
    "TD",   # flake8-todos
    "TID",  # flake8-tidy-imports
    "TRY",  # tryceratops
    "UP",   # pyupgrade
    "W",    # pycodestyle waring
    # "ASYNC", # flake8-async
    # "DJ",    # flake8-django
    # "EXE",   # flake8-executable
    # "INT",   # flake8-gettext
    # "PD",    # pandas-vet
    # "YTT",   # flake8-2020
]
ignore = [
    "ANN101",  # flake8-annotations: missing-type-self
    "ANN102",  # flake8-annotations: missing-type-cls
    "ARG001",  # flake8-unused-arguments: unused-function-argument
    "COM812",  # flake8-commas: missing-trailing-comma
    "D107",    # undocumented-public-init
    "D401",    # pydocstyle: non-imperative-mood
    "PLR2004", # Pylint:magic-value-comparison
    "RUF001",  # Ruff-specific rules: ambiguous-unicode-character-string
]

[tool.ruff.per-file-ignores]
"docs/conf.py" = [
    "INP001", # import
]


[tool.ruff.lint.pylint]
max-positional-args = 5
max-args = 8

[tool.ruff.lint.flake8-quotes]
inline-quotes = "single"

[tool.ruff.lint.pydocstyle]
# Use numpy-style docstrings.
convention = "numpy"
