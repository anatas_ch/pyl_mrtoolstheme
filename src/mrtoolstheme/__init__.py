"""Register the Sphinx HTML Theme `mrtools`.

.. moduleauthor:: Michael Rippstein <info@anatas.ch>

:copyright: Copyright 2022 by Michael Rippstein.
:license: AGPL-3.0-or-later, see LICENSE for details.
"""

from pathlib import Path

import sphinx


def setup(app: sphinx.application.Sphinx) -> None:
    """Register the Sphinx HTML Theme `mrtools`."""
    app.add_html_theme('mrtools', Path(__file__).resolve().parent)
