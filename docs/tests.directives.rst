=====================
Test Cases Directives
=====================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Paragraph-level markup
======================

.. rst:directive:: versionadded

.. versionadded:: 2.5
   The *spam* parameter.


.. rst:directive:: versionchanged

.. versionchanged:: 0.2
   Changed type of the parameter ``foo``.


.. rst:directive:: deprecated

.. deprecated:: 3.1
   Use :func:`spam` instead.


.. rst:directive:: seealso
   :noindex:

.. seealso::

   Module :py:mod:`zipfile`
      Documentation of the :py:mod:`zipfile` standard module.

   `GNU tar manual, Basic Tar Format <http://link>`_
      Documentation for tar archive files, including GNU tar extensions.


.. rst:directive:: seealso
   :noindex:

.. seealso:: modules :py:mod:`zipfile`, :py:mod:`tarfile`
